:warning: :warning: :warning:

Everything here is a work in progress. I cannot stress enough that it doesn't
actually work. The pieces are an adaptation of an older project which had a
different structure, I've renamed things in some places but not others, etc. So
everything is broken. I have put it up so that another project can see some of
what's involved, theoretically, but again — nothing here will work out of the
box.

# Raspbian Live

This project uses Debian's live image building tool `live-build` to create live Raspbian images that run on the Raspberry Pi. Perhaps more importantly, it demonstrates and documents the whole process and design, start to finish, so that other developers can build on it and create their own live systems for the Raspberry Pi.

Live images allow you to instantly switch your Raspberry Pi from one use to another just by switching SD cards, to carry your OS and data with you on removable media to use on other machines, or just prototype new uses without affecting your usual environment.

See the wiki to learn more about:

  * What is the Raspberry Pi?
  * What is a live image?
  * How does Debian come into it?

# Credit

This is all based hugely on
[Simon Poole's work](https://github.com/simonpoole1/raspbian-live-build) on a
Raspbian Live Build system.

# Dependencies

The project depends primarily on the latest released version of Debian's Live Build tools ie. the `live-build` package in the unstable distribution (`sid`), whatever version that may be. If a new version of `live-build` is released that breaks this project, it should be considered a issue with this project (unless it's an obvious bug in `live-build`).

The full list of dependencies are:

  * Debian packages (see notes below):
    * `live-build`
    * `qemu-user-static`
  * A Raspbian package repository
  * For testing:
    * Qemu's ARM emulation binaries (unsure which, see notes below)

Obviously if you want to try it out on the hardware itself, you will need a Raspberry Pi of generation 2 or higher.

## Note about dependency on Debian tools

I've pegged the version of Live Build that this project depends on to be "whatever's currently in `sid`". But you may not actually want to develop on Debian's unstable distribution (I don't), and it may not be possible to install the latest version of Live Build on your preferred environment. See the wiki for notes on using Live Build in eg. a virtual machine.

## Note about dependency on Qemu

I'm not yet sure what the minimum version required for running a Raspberry Pi image is. Because I haven't got the image fully working yet, there's no easy way for me to know whether it's because Qemu doesn't support something or because my image is faulty. So my current process requires building Qemu out of their Github reository. I have instructions for that on the wiki.

# Building the image

All building is controlled with GNU Make via the `makefile`.

First you need to populate the configuration files that `live-build` requires:

    $ make config

This produces a tree of directories and files that tell `live-build` what to build, what repositories to use, what files to populate the resulting image with, and all the other things it needs 

Then you can build the live image:

    $ make

You should end up with three binary files:

  1. `pi-minimal.img` - this is the image that goes on an SD card, that the RPi boots from.
  
  2. `pi-minimal-initrd.img-N.MM.L-P-rpi` - this is extracted from the above, it's the initial ramdisk image required for running the image on Qemu. `N.MM.L-P` is a kernel package version number.

  3. `pi-minimal-vmlinuz-N.MM.L-P-rpi` - this is the kernel image, also extracted from the disk image, needed by Qemu.

# Running the image on the Raspberry Pi

Write the image to an SD card (it should be unmounted but not ejected):

    $ sudo dd if=pi-minimal.img of=/dev/SDCARDDEV status=progress && sync

`SDCARDDEV` is the device node of your SD card. Eject the SD card, insert it into the RPi and power it up.

# Running the image on Qemu

You will need to extract some files from the disk image to be able to boot the image with Qemu. I reccomend using `kpartx`:

    sudo kpartx -v -a pi-minimal.img
    mkdir boot root

These are the map devices shown by the `kpartx` command:

    $ sudo mount /dev/mapper/loop1p1 boot
    $ sudo mount /dev/mapper/loop0p2 root

Copy out the kernel and device configuration:

    $ cp boot/kernel7.img .
    $ cp boot/bcm2709-rpi-2-b.dtb .

Clean everything up:

    $ sudo umount root
    $ sudo umount boot

    $ sudo kpartx -d pi-minimal.img

    $ sudo rmdir boot/
    $ sudo rmdir root

Run Qemu: 

    qemu-system-arm -M raspi2 -kernel kernel7.img -sd pi-minimal.img -append "rw earlyprintk loglevel=8 console=ttyAMA0,115200 dwc_otg.lpm_enable=0 root=/dev/mmcblk0p2" -dtb bcm2709-rpi-2-b.dtb -serial stdio

Qemu will boot the image, providing a serial console on your terminal and the graphical display in a separate window.
