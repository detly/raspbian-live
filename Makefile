BUILD_LOG := build.log

.PHONY: clean

config:
	env DEBOOTSTRAP_OPTIONS="--keyring=/usr/share/keyrings/raspbian-archive-keyring.gpg" \
	lb config

pi-minimal.img:
	sudo lb build

clean:
	sudo lb clean
	sudo lb clean --purge
